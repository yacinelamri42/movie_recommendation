#!/usr/bin/env sh

# /* Assignement 1
#  * Yacine Lamri: 40285309
#  */

PROJECT_ROOT=$(dirname $0)
CC=gcc
# CFLAGS="-Wall -Wextra -Wpedantic -g -std=c11 -fsanitize=address"
CFLAGS="-Wall -g -std=c11 -fsanitize=address"
# CFLAGS="-std=c11 -O2"
APPNAME="movie_recommend"
[ -d $PROJECT_ROOT/bin ] || mkdir $PROJECT_ROOT/bin/
$CC $CFLAGS -o $PROJECT_ROOT/bin/$APPNAME $PROJECT_ROOT/src/*.c
