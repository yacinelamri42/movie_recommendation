/* Assignement 1
 * Yacine Lamri: 40285309
 */

#ifndef READ_LINE__H
#define READ_LINE__H

#include <stddef.h>

char* read_line(size_t* line_size,const char* string, size_t size_of_string);
char* readline(size_t* line_size,const char* string, size_t size_of_string);

#endif // !READ_LINE__H
