/* Assignement 1
 * Yacine Lamri: 40285309
 */

#include "movie.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Movie movie_create(char* title, char* genre, float rating) {
	Movie movie = {0};
	size_t title_length = strlen(title);
	size_t genre_length = strlen(genre);
	char* t = calloc(title_length+1, sizeof(char));
	char* g = calloc(genre_length+1, sizeof(char));
	if (!t || !g) {
		return movie;
	}
	movie.genre = g;
	movie.title = t;
	movie.genre_length = genre_length;
	movie.title_length = title_length;
	movie.title_alloc_size = title_length+1;
	movie.genre_alloc_size = genre_length+1;
	movie.rating = rating;
	memcpy(movie.genre, genre, genre_length);
	memcpy(movie.title, title, title_length);
	return movie;
}

void print_movie(Movie movie) {
	printf("%s (%s) - %1.1f", movie.title, movie.genre, movie.rating);
}

void movie_destroy(Movie* movie) {
	free(movie->genre);
	free(movie->title);
	movie->title = 0;
	movie->genre = 0;
	movie->rating = 0;
	movie->title_length = 0;
	movie->title_alloc_size = 0;
	movie->genre_length = 0;
	movie->genre_alloc_size = 0;
}
