/* Assignement 1
 * Yacine Lamri: 40285309
 */

#include "user.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

User user_create(char* name, float* ratings, size_t ratings_length) {
	User user = {0};
	size_t name_length = strlen(name);
	char* n = calloc(name_length+1, sizeof(char));
	float* r = calloc(ratings_length, sizeof(float));
	if (!n || !r) {
		return user;
	}
	user.name = n;
	user.name_length = name_length;
	user.name_alloc_size = name_length+1;
	user.ratings = r;
	user.ratings_length = ratings_length;
	user.ratings_alloc_size = ratings_length*sizeof(float);
	if (!ratings) {
		for (int i=0; i<ratings_length; i++) {
			r[i] = 0;
		}
		ratings = r;
	}
	memcpy(user.ratings, ratings, ratings_length*sizeof(float));
	memcpy(user.name, name, name_length*sizeof(char));
	user.name[user.name_length] = 0;
	return user;
}

void print_user(User user) {
	printf("Name: %s\n", user.name);
	printf("Rating: ");
	for (int i=0; i<user.ratings_length; i++) {
		printf("%1.1f ", user.ratings[i]);
	}
	printf("\n");
}

void user_destroy(User* user) {
	free(user->name);
	free(user->ratings);
	user->ratings = 0;
	user->name = 0;
	user->ratings_length = 0;
	user->ratings_alloc_size = 0;
	user->name_alloc_size = 0;
}
