/* Assignement 1
 * Yacine Lamri: 40285309
 */

#ifndef STATE__H
#define STATE__H

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include "user.h"
#include "movie.h"

#define MOVIE_DATABASE "movie_database.txt"
#define USER_DATA "user_data.txt"
#define USER_RATINGS "user_ratings.txt"

typedef struct {
	User* users;
	size_t number_of_users;
	size_t number_allocated_of_users;
	Movie* movies;
	size_t number_of_movies;
	size_t number_allocated_of_movies;
} ProgramState;

typedef enum {
	PROGRAM_ERROR_NONE,
	PROGRAM_MALLOC_ERROR,
	PROGRAM_FILE_ERROR,
	PROGRAM_WRONG_VALUES_ERROR,
} ProgramError;

void print_program_error(ProgramError err);
ProgramError load_program_state(ProgramState* state);
ProgramError save_program_state(ProgramState* state);
ProgramError add_user(ProgramState* state, const char* name, size_t size);
int get_user_id(ProgramState* state, const char* name, size_t size);
ProgramError set_user_rating(ProgramState* state, const char* name, size_t size, float rating, size_t movie_index);
void free_state(ProgramState* state);

#endif // !STATE__H
