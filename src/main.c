/* Assignement 1
 * Yacine Lamri: 40285309
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "movie.h"
#include "state.h"
#include "read_line.h"
#include "util.h"

void display_choices(void);

ProgramError register_user(ProgramState* state);
void display_movies(ProgramState* state);
ProgramError rate_movie(ProgramState* state);
ProgramError get_movie_recommendation(ProgramState* state);

int main(int argc, char *argv[]){
	ProgramState state = {0};
	ProgramError err = load_program_state(&state);
	if (err) {
		print_program_error(err);
		return 1;
	}
	while (1) {
		display_choices();
		size_t input_size = 0;
		char* input = readline(&input_size, "", 0);
		if (!input) {
			break;
		}
		if (input_size == 0) {
			free(input);
			continue;
		}
		if (input[0] == '0') {
			free(input);
			break;
		}
		switch (input[0]) {
			case '1':
				err = register_user(&state);
				print_program_error(err);
			break;
			case '2':
				display_movies(&state);
			break;
			case '3':
				err = rate_movie(&state);
				print_program_error(err);
			break;
			case '4':
				err = get_movie_recommendation(&state);
				print_program_error(err);
			break;
			default:
				printf("Command not found\n");
			break;
		}
		free(input);
	}
	free_state(&state);
	return 0;
}

ProgramError register_user(ProgramState* state) {
	ProgramError err = PROGRAM_ERROR_NONE;
	do{
		size_t input_size = 0;
		char prompt[] = "Enter username for registration: ";
		char* input = readline(&input_size, prompt, strlen(prompt));
		if (!input) {
			return PROGRAM_MALLOC_ERROR;
		}
		err = add_user(state, input, input_size);
		if (err == PROGRAM_WRONG_VALUES_ERROR) {
			printf("User already exists. Please choose a different name.\n");
		}
		free(input);
	}while (err);
	return err;
}

void display_movies(ProgramState* state) {
	printf("\n***** Movies Database *****\n");
	for (int i=0; i<state->number_of_movies; i++) {
		printf("%d. ", i+1);
		print_movie(state->movies[i]);
		printf("\n");
	}
	printf("\n");
}

ProgramError rate_movie(ProgramState* state) {
	size_t username_size = 0;
	char username_prompt[] = "Enter your username: ";
	char* username = readline(&username_size, username_prompt, sizeof(username_prompt));
	if (!username) {
		return PROGRAM_MALLOC_ERROR;
	}
	if (get_user_id(state, username, username_size) == -1) {
		printf("User not found. Please register first.");
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	display_movies(state);
	char movie_index_prompt[] = "Enter the number of the movie you want to rate: ";
	size_t movie_index_size = 0;
	char* movie_index_str = readline(&movie_index_size, movie_index_prompt, sizeof(movie_index_prompt));
	if (!movie_index_str) {
		free(username);
		return PROGRAM_MALLOC_ERROR;
	}
	int movie_index = 0;
	if(string_to_int(movie_index_str, movie_index_size, &movie_index)) {
		free(movie_index_str);
		free(username);
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	if (movie_index-1 >= state->number_of_movies) {
		free(username);
		free(movie_index_str);
		printf("%d is not between 1 and 5\n", movie_index+1);
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	char rating_prompt[] = "Enter your rating (1-5): ";
	size_t rating_size = 0;
	char* rating_str = readline(&rating_size, rating_prompt, sizeof(rating_prompt));
	if (!rating_str) {
		free(username);
		free(movie_index_str);
		return PROGRAM_MALLOC_ERROR;
	}
	float rating = 0;
	if (string_to_float(rating_str, rating_size, &rating)) {
		free(rating_str);
		free(username);
		free(movie_index_str);
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	ProgramError err = set_user_rating(state, username, username_size, rating, movie_index-1);
	free(rating_str);
	free(movie_index_str);
	free(username);
	return err;
}

ProgramError get_movie_recommendation(ProgramState* state) {
	float* average_rating_per_movie_not_watched = calloc(state->number_of_movies, sizeof(float));
	if (!average_rating_per_movie_not_watched) {
		return PROGRAM_MALLOC_ERROR;
	}
	char username_prompt[] = "Enter your username: ";
	size_t username_size = 0;
	char* username = readline(&username_size, username_prompt, sizeof(username_prompt));
	if (!username) {
		free(average_rating_per_movie_not_watched);
		return PROGRAM_MALLOC_ERROR;
	}
	int user_index = get_user_id(state, username, username_size);
	if (user_index == -1) {
		free(username);
		free(average_rating_per_movie_not_watched);
		printf("User not found. Please register first.");
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	for (int i=0; i<state->number_of_movies; i++) {
		if (!state->users[user_index].ratings[i]) {
			int num_users_that_have_rated = 0;
			for (int j=0; j<state->number_of_users; j++) {
				if (state->users[j].ratings[i] != 0) {
					average_rating_per_movie_not_watched[i]+=state->users[j].ratings[i];
					num_users_that_have_rated++;
				}
			}
			if (num_users_that_have_rated) {
				average_rating_per_movie_not_watched[i] = average_rating_per_movie_not_watched[i]/num_users_that_have_rated;
			}
		}
	}
	printf("***** Recommended Movies *****\n");
	int index = 1;
	for (int i=0; i<state->number_of_movies; i++) {
		if (average_rating_per_movie_not_watched[i]) {
			printf("%d. %s (%s) - Predicted rating: %1.1f\n", index++, state->movies[i].title, state->movies[i].genre, average_rating_per_movie_not_watched[i]);
		}
	}
	printf("\n");
	free(username);
	free(average_rating_per_movie_not_watched);
	return PROGRAM_ERROR_NONE;
}

void display_choices(void) {
	printf("***** Movie Recommendation System *****\n");
	printf("\t1. Register User\n");
	printf("\t2. Display movies\n");
	printf("\t3. Rate a movie\n");
	printf("\t4. Get a movie recommendation\n");
	printf("\t0. Exit\n");
	printf("Enter your choice: > ");
}
