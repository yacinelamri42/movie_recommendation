/* Assignement 1
 * Yacine Lamri: 40285309
 */

#ifndef MOVIE__H
#define MOVIE__H

#include <stddef.h>

typedef struct {
	char* title;
	size_t title_length;
	size_t title_alloc_size;
	char* genre;
	size_t genre_length;
	size_t genre_alloc_size;
	float rating;
} Movie;

Movie movie_create(char* title, char* genre, float rating);
void print_movie(Movie movie);
void movie_destroy(Movie* movie);

#endif // !MOVIE__H
