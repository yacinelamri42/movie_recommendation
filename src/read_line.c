/* Assignement 1
 * Yacine Lamri: 40285309
 */

#include "read_line.h"
#include <stdio.h>
#include <stdlib.h>

char* readline(size_t* line_size,const char* string, size_t size_of_string) {
	printf("%s", string);
	char* buffer = calloc(1024, sizeof(char));
	if (!buffer) {
		return 0;
	}
	size_t alloc_size = 1024;
	char c;
	int i;
	for (i=0; i<alloc_size && (c=fgetc(stdin))!='\n'; i++) {
		if (c == EOF) {
			free(buffer);
			return 0;	
		}
		if ((alloc_size-i)<16) {
			char* t = realloc(buffer, (alloc_size+256)* sizeof(char));
			if (!t) {
				free(buffer);
				return 0;
			}
			alloc_size+=256;
			buffer = t;
		}
		buffer[i] = c;
	}
	buffer[i] = 0;
	*line_size = i;
	return buffer;
}
