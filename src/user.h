/* Assignement 1
 * Yacine Lamri: 40285309
 */

#ifndef USER__H
#define USER__H

#include <stddef.h>

typedef struct {
	char* name;
	size_t name_length;
	size_t name_alloc_size;
	float* ratings;
	size_t ratings_length;
	size_t ratings_alloc_size;
} User;

User user_create(char* name, float* ratings, size_t ratings_length);
void print_user(User user);
void user_destroy(User* user);

#endif // USER__H
