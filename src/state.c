/* Assignement 1
 * Yacine Lamri: 40285309
 */

#include "state.h"
#include "movie.h"
#include "user.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

typedef int Error ;
static size_t count_lines(FILE* file) {
	fseek(file, 0, SEEK_SET);
	size_t size = 0;
	size_t num_lines = 0;
	for (char c=0; (c=fgetc(file))!=EOF; size++) {
		if (c=='\n') {
			num_lines++;
		}
	}
	return num_lines;
}

static char* split_line(char* line, size_t line_size, char delimiter, size_t index_of_element_desired, size_t* item_size) {
	size_t current_index = 0;
	int ite_size = 0;
	int pos = -1;
	for (int i=0; i<line_size; i++) {
		if (line[i] == delimiter) {
			current_index++;
			continue;
		}
		if (current_index == index_of_element_desired) {
			if (pos == -1) {
				pos = i;
			}
			ite_size++;
		}
		if (current_index > index_of_element_desired) {
			break;
		}
	}
	if (ite_size == 0) {
		return 0;
	}
	assert(ite_size<=line_size);
	assert((pos+ite_size)<=line_size);
	int item_pos = 0;
	char* item = calloc(ite_size+1, sizeof(char));
	if (!item) {
		return 0;
	}
	for (int i=pos; i<(pos+ite_size); i++) {
		item[item_pos++] = line[i];
	}
	item[ite_size] = 0;
	*item_size = ite_size;
	return item;
}

static char* get_line(FILE* file, size_t* size, size_t line_number) {
	fseek(file, 0, SEEK_SET);
	size_t line_length = 0;
	size_t current_line = 0;
	int pos = -1;
	char c=0;
	for (int i=0; (c=fgetc(file))!=EOF; i++) {
		if (c == '\n') {
			current_line++;
			continue;
		}
		if (current_line > line_number) {
			break;
		}
		if (current_line == line_number) {
			if (pos == -1) {
				pos = i;
			}
			line_length++;
		}
	}
	fseek(file, 0, SEEK_SET);
	if (pos == -1) {
		return 0;
	}
	char* line = calloc(line_length+1, sizeof(char));
	if (!line) {
		return line;
	}
	fseek(file, pos, SEEK_SET);
	for (int i=0; i<line_length-1; i++) {
		line[i] = fgetc(file);
	}
	line[line_length] = 0;
	if (size) {
		*size = line_length-1;
	}
	return line;
}

bool string_compare(char* str1, size_t size_of_str1, char* str2, size_t size_of_str2) {
	if (size_of_str1 != size_of_str2) {
		return false;
	}
	for (int i=0; i<size_of_str2; i++) {
		if ((str1[i] | 1<<5) != (str2[i] | 1<<5)) {
			return false;
		}
	}
	return true;
}

int get_user_id(ProgramState* state, const char* name, size_t size) {
	for (int i=0; i<state->number_of_users; i++) {
		if (string_compare(state->users[i].name, state->users[i].name_length, (char*)name, size)) {
			return i;
		}
	}
	return -1;
}

ProgramError add_user(ProgramState* state, const char* name, size_t size) {
	if (get_user_id(state, name, size) != -1) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}

	if (state->number_of_users == state->number_allocated_of_users) {
		User* users = realloc(state->users, (state->number_allocated_of_users+1)*sizeof(User));
		if (!users) {
			return PROGRAM_MALLOC_ERROR;
		}
		state->users = users;
		state->number_allocated_of_users++;
	}
	state->users[state->number_of_users] = user_create((char*)name, 0, state->number_of_movies);
	if (!(state->users[state->number_of_users].name)) {
		return PROGRAM_MALLOC_ERROR;
	}
	state->number_of_users++;
	save_program_state(state);
	return PROGRAM_ERROR_NONE;
}

ProgramError set_user_rating(ProgramState* state, const char* name, size_t size, float rating, size_t movie_index) {
	if (rating > 5) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	int id = get_user_id(state, name, size);
	if (id == -1) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	if (movie_index >= state->number_of_movies) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	state->users[id].ratings[movie_index] = rating;
	save_program_state(state);
	return PROGRAM_ERROR_NONE;
}

static ProgramError load_users(User** users, size_t* num_users, size_t* alloc_size, size_t num_movies) {
	FILE* file = fopen(USER_DATA, "r");
	if (!file) {
		return PROGRAM_FILE_ERROR;
	}
	size_t num_lines = count_lines(file);
	char** names = calloc(num_lines, sizeof(char*));
	if (!names) {
		return PROGRAM_MALLOC_ERROR;
	}
	User* the_users = calloc(num_lines, sizeof(User));
	if (!the_users) {
		return PROGRAM_MALLOC_ERROR;
	}
	for (int i=0; i<num_lines; i++) {
		size_t line_size = 0;
		char* line = get_line(file, &line_size, i);
		if (!line) {
			return PROGRAM_MALLOC_ERROR;
		}
		size_t name_size = 0;
		char* name = split_line(line, line_size, ' ', 0, &name_size);
		if (!name) {
			return PROGRAM_MALLOC_ERROR;
		}
		names[i] = name;
		free(line);
	}
	fclose(file);
	file = fopen(USER_RATINGS, "r");
	if (!file) {
		return PROGRAM_FILE_ERROR;
	}
	size_t line_size = 0;
	size_t item_size = 0;
	size_t item2_size = 0;
	char* line = get_line(file, &line_size, 0);
	char* num_users_from_ratings_str = split_line(line, line_size, ' ', 0, &item_size);
	char* num_movies_from_ratings_str = split_line(line, line_size, ' ', 1, &item2_size);
	free(line);
	int num_users_from_ratings = 0;
	int num_movies_from_ratings = 0;
	if(string_to_int(num_movies_from_ratings_str, item2_size, &num_movies_from_ratings)) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	if(string_to_int(num_users_from_ratings_str, item_size, &num_users_from_ratings)) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	if (num_users_from_ratings != num_lines || num_movies_from_ratings != num_movies) {
		return PROGRAM_WRONG_VALUES_ERROR;
	}
	free(num_users_from_ratings_str);
	free(num_movies_from_ratings_str);
	for (int i=0; i<num_lines; i++) {
		line = get_line(file, &line_size, i+1);
		if (!line) {
			return PROGRAM_MALLOC_ERROR;
		}
		float* row = calloc(num_movies_from_ratings, sizeof(float));
		if (!row) {
			return PROGRAM_MALLOC_ERROR;
		}
		for (int j=0; j<num_movies_from_ratings; j++) {
			size_t rating_size = 0;
			char* rating_str = split_line(line, line_size, ' ', j, &rating_size);
			if (!rating_str) {
				return PROGRAM_MALLOC_ERROR;
			}
			float rating = 0;
			if (string_to_float(rating_str, rating_size, &rating)) {
				return PROGRAM_WRONG_VALUES_ERROR;
			}
			row[j] = rating;
			free(rating_str);
		}
		the_users[i] = user_create(names[i], row, num_movies_from_ratings);
		if (!the_users[i].name) {
			return PROGRAM_MALLOC_ERROR;
		}
		free(row);
		free(line);
	}
	for (int i=0; i<num_lines; i++) {
		free(names[i]);
	}
	free(names);
	fclose(file);
	*users = the_users;
	*num_users = num_lines;
	*alloc_size = num_lines;
	return PROGRAM_ERROR_NONE;
}

static ProgramError load_movies(Movie** movies, size_t* num_movies) {
	FILE* file = fopen(MOVIE_DATABASE, "r");
	if (!file) {
		return PROGRAM_FILE_ERROR;
	}
	size_t num_lines = count_lines(file);
	Movie* movies_db = calloc(num_lines, sizeof(Movie));
	if (!movies_db) {
		return PROGRAM_MALLOC_ERROR;
	}
	for (int i=0; i<num_lines; i++) {
		size_t line_size = 0;
		char* line = get_line(file, &line_size, i);
		if (!line) {
			return PROGRAM_MALLOC_ERROR;
		}
		size_t title_size = 0;
		size_t genre_size = 0;
		size_t rating_size = 0;
		char* title = split_line(line, line_size, ' ', 0, &title_size);
		if (!title) {
			return PROGRAM_MALLOC_ERROR;
		}
		for (int j=0; j<title_size; j++) {
			if (title[j] == '_') {
				title[j] = ' ';
			}
		}
		char* genre = split_line(line, line_size, ' ', 1, &genre_size);
		if (!genre) {
			return PROGRAM_MALLOC_ERROR;
		}
		char* rating_str = split_line(line, line_size, ' ', 2, &rating_size);
		if (!rating_str) {
			return PROGRAM_MALLOC_ERROR;
		}
		float rating = 0;
		if (string_to_float(rating_str, rating_size, &rating)) {
			return PROGRAM_WRONG_VALUES_ERROR;
		}
		movies_db[i] = movie_create(title, genre, rating);
		free(rating_str);
		free(title);
		free(genre);
		free(line);
	}
	fclose(file);
	*num_movies = num_lines;
	*movies = movies_db;
	return PROGRAM_ERROR_NONE;
}

void free_state(ProgramState* state) {
	for (int i=0; i<state->number_of_movies; i++) {
		movie_destroy(state->movies+i);
	}
	free(state->movies);
	state->movies = 0;
	state->number_of_movies = 0;
	state->number_allocated_of_movies = 0;
	for (int i=0; i<state->number_of_users; i++) {
		user_destroy(state->users+i);
	}
	free(state->users);
	state->users = 0;
	state->number_of_users = 0;
	state->number_allocated_of_users = 0;
}

void print_program_error(ProgramError err) {
	switch (err) {
		case PROGRAM_ERROR_NONE:
		break;
		case PROGRAM_FILE_ERROR:
			printf("file error\n");
		break;
		case PROGRAM_MALLOC_ERROR:
			printf("malloc error\n");
		break;
		case PROGRAM_WRONG_VALUES_ERROR:
			printf("wrong values error\n");
		break;
	}
}

ProgramError save_users(User* users, size_t num_users, size_t num_movies) {
	FILE* file = fopen(USER_DATA, "w");
	for (int i=0; i<num_users; i++) {
		fprintf(file, "%s %d\r\n", users[i].name, i+1);
	}
	fclose(file);
	file = fopen(USER_RATINGS, "w");
	assert(num_movies == users[0].ratings_length);
	fprintf(file, "%ld %ld\r\n", num_users, num_movies);
	for (int i=0; i<num_users; i++) {
		for (int j=0; j<num_movies; j++) {
			fprintf(file, "%1.1f ", users[i].ratings[j]);
		}
		fprintf(file, "\r\n");
	}
	fclose(file);
	return PROGRAM_ERROR_NONE;
}

ProgramError load_program_state(ProgramState* state) {
	User* users = 0;
	size_t num_users = 0;
	size_t users_alloc_size = 0;
	Movie* movies = 0;
	size_t num_movies = 0;
	ProgramError m = load_movies(&movies, &num_movies);
	if(m != PROGRAM_ERROR_NONE) {
		return m;	
	}
	print_program_error(m);
	// for (int i=0; i<num_movies; i++) {
	// 	printf("%d. ", i+1);
	// 	print_movie(movies[i]);
	// 	putchar('\n');
	// }
	ProgramError u = load_users(&users, &num_users, &users_alloc_size, num_movies);
	if(u != PROGRAM_ERROR_NONE) {
		return u;	
	}
	state->movies = movies;
	state->users = users;
	state->number_of_users = num_users;
	state->number_of_movies = num_movies;
	state->number_allocated_of_users = num_users;
	state->number_allocated_of_movies = num_movies;
	print_program_error(u);
	return PROGRAM_ERROR_NONE;
}

ProgramError save_program_state(ProgramState* state) {
	return save_users(state->users, state->number_of_users, state->number_of_movies);
}
