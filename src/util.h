/* Assignement 1
 * Yacine Lamri: 40285309
 */

#ifndef UTIL__H
#define UTIL__H

typedef int Error;
#include <stddef.h>

float power(int base, int power);
Error string_to_int(char* string, size_t size_of_string, int* num);
Error string_to_float(char* string, size_t size_of_string, float* num);

#endif // !UTIL__H
