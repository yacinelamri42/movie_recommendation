/* Assignement 1
 * Yacine Lamri: 40285309
 */
#include "util.h"

float power(int base, int power) {
	float r = 1;
	int op = 0;
	if (power < 0) {
		op = 1;
		power = -power;
	}
	for (int i=0; i<power; i++) {
		if (op) {
			r/=base;
		}else{
			r*=base;
		}
	}
	return r;
}

Error string_to_float(char* string, size_t size_of_string, float* num) {
	int start = 0;
	if (string[0] == '-') {
		start = 1;
	}
	int num_int_digit = 0;
	int num_decimal_digit= 0;
	int had_point = 0;
	float n = 0;
	for (int i=start; i<size_of_string; i++) {
		if ((string[i]<'0'||string[i]>'9') && string[i] !='.') {
			return 1;
		}
		if (string[i]=='.' && had_point) {
			return 1;
		}
		if (string[i]=='.' && !had_point) {
			had_point = 1;
			continue;
		}
		if (had_point) {
			num_decimal_digit++;
		}else {
			num_int_digit++;
		}
	}
	for (int i=start; i<size_of_string; i++) {
		if (string[i] == '.') {
			num_int_digit++;
			continue;
		}
		n+=(string[i]-'0')*power(10, num_int_digit-i-1);
	}
	if (start) {
		n = -n;
	}
	*num = n;
	return 0;
}

// return 1 if problem, return 0 if works
Error string_to_int(char* string, size_t size_of_string, int* num) {
	int start = 0;
	if (string[0] == '-') {
		start = 1;
	}
	int n = 0;
	int p = size_of_string-start-1;
	for (int i=start; i<size_of_string; i++) {
		if (string[i]<'0' || string[i]>'9') {
			return 1;
		}else {
			if (start) {
				n-=(string[i]-'0')*(int)power(10, p);
			}else {
				n+=(string[i]-'0')*(int)power(10, p);
			}
		}
	}
	*num = n;
	return 0;
}

